<?php namespace Showcase;

use League\CommonMark\CommonMarkConverter;
use Symfony\Component\Yaml\Yaml;

class WorkLoader {

    /**
     * @var Parsedown
     */
    private $md_parser;

    private $lang;
    private $content_dir;

    /**
     *
     * @param CommonMarkConverter $md_parser
     * @param string $content_dir Directory from which .work files will be loaded.
     * @param string|null $lang null = will only read base file for specified slug.
     */
    public function __construct(CommonMarkConverter $md_parser, $content_dir, $lang = null)
    {
        $this->lang = $lang;
        $this->md_parser = $md_parser;
        $this->content_dir = $content_dir;
    }

    public function exists($name) {
        return file_exists($this->content_dir . "/$name.work");
    }

    /**
     * @param string $name
     * @param resource &$content_handle Handle for file that is seeked to correct position for contents reading. Needs to be closed by caller!
     */
    public function load_info($name, &$content_handle) {
        $base_filename = "$name.work";

        $handle = fopen($this->content_dir . "/$base_filename", 'r');
        $base_info = $this->read_header($handle);

        $this->apply_language($base_info);

        $content_handle = $handle;

        return $base_info;
    }

    public function load($name) {
        $handle   = null;

        $info     = $this->load_info($name, $handle);
        $contents = $this->read_contents($handle);

        fclose($handle);

        return [
            'info' => $info,
            'contents' => $contents
        ];
    }

    private function read_header($handle, $separator = "==") {
        $line = '';
        $data = '';

        while (!strstr($line, $separator) && $line !== false) {
            $data .= $line;
            $line = fgets($handle);
        }

        return Yaml::parse($data);
    }

    private function apply_language(array &$info) {
        array_walk($info, function (&$value, $key) use($info) {
            if (array_key_exists($key . "_$this->lang", $info))
                $value = $info[$key . "_$this->lang"];
        });
    }

    const SKIP = 0;
    const BASE = 1;
    const LANG = 2;

    /**
     * This function presumes that the provided handle is seeked to
     * correct position for reading.
     */
    private function read_contents($handle) {
        $line = '';

        $state = WorkLoader::BASE;

        $base_data = '';
        $lang_data = '';

        while (!feof($handle)) {
            if (strstr($line, "==lang:")) {
                $parsed_lang = substr($line, 7, 2);
                if ($this->lang === $parsed_lang) {
                    // Required language has already been parsed, no need to continue.
                    if (strlen($lang_data) > 0) break;

                    $state = WorkLoader::LANG;
                    $line = fgets($handle);
                } else {
                    $state = WorkLoader::SKIP;
                }
            }

            switch ($state) {
                case WorkLoader::BASE:
                $base_data .= $line;
                break;
                case WorkLoader::LANG:
                $lang_data .= $line;
                break;
                case WorkLoader::SKIP:
                /** do not store the line... */
                break;
            }

            $line = fgets($handle);
        }

        return $this->md_parser->convertToHtml(strlen($lang_data) > 0 ? $lang_data : $base_data);
    }

}
