<?php namespace Showcase;

use League\CommonMark\CommonMarkConverter;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

use Twig\Loader\FilesystemLoader;
use Twig\Environment;

class App {

    private $config;
    private $aliases;

    /**
     * @var FilesystemLoader $twig_loader
     */
    private $twig_loader;

    /**
     * @var Environment $twig_env
     */
    private $twig_env;

    /**
     * @var Finder $finder
     */
    private $finder;

    const DEFAULT_LANG = 'en';

    private $lang = null;
    private $lang_map = array();
    private $available_langs = ['en'];

    private $work_info_list = [];

    public function __construct($config, $aliases)
    {
        $this->config = $config;
        $this->aliases = $aliases;

        if ($this->config['debug']) {
            error_reporting(E_ALL);
            ini_set("display_errors", 1);
        }

        $lang_finder = (new Finder())->in($this->config['lang_dir'])->files()->name('*.yaml');

        foreach ($lang_finder as $lang_file) {
            $this->available_langs[] = $lang_file->getBasename('.yaml');
        }

        $this->lang = $this->config['lang'] ? $this->config['lang'] : App::DEFAULT_LANG;

        if ($this->lang && $this->lang != App::DEFAULT_LANG && file_exists($this->config['lang_dir'] . "/$this->lang.yaml"))
            $this->lang_map = Yaml::parseFile($this->config['lang_dir'] . "/$this->lang.yaml");

        $this->rewrite = $this->config['rewrite'];
        $this->explicit_lang_url = $this->config['explicit_lang_url'];

        $this->twig_loader = new FileSystemLoader($this->config['view_dir']);
        $this->twig_env    = new Environment($this->twig_loader);

        $this->md_parser   = new CommonMarkConverter();
        $this->finder      = (new Finder())->in($this->config['content_dir'])->files()->name('*.work')->notName('/^_(.*)$/');
        $this->work_loader = new WorkLoader($this->md_parser, $this->config['content_dir'], $this->lang);

        $this->requested_work = htmlspecialchars($_GET['work']);
        $this->requested_sitemap = htmlspecialchars($_GET['sitemap']) === '1';

        if (array_key_exists($this->lang, $this->aliases) && array_key_exists($this->requested_work, $this->aliases[$this->lang])) {
            $this->requested_work = $this->aliases[$this->lang][$this->requested_work];
        }

        $this->register_twig_extensions();
    }

    public function register_twig_extensions() {
        $filter_work_url = new \Twig\TwigFilter('work_url', function ($content, $lang = null) { return $this->get_work_url($content, $lang); });

        $filter_translate = new \Twig\TwigFilter('_', function ($content) { return $this->translate($content); });

        $filter_alternate_url = new \Twig\TwigFilter('alt_url', function ($content) { return $this->alternate_url($content); });

        $filter_lang_work_alias = new \Twig\TwigFilter('lang_work_alias', function ($content, $lang) { return $this->get_lang_work_alias($content, $lang); });

        $filter_to_absolute = new \Twig\TwigFilter('to_absolute', function ($content) { return $this->to_absolute($content); });

        $this->twig_env->addFilter($filter_work_url);
        $this->twig_env->addFilter($filter_translate);
        $this->twig_env->addFilter($filter_alternate_url);
        $this->twig_env->addFilter($filter_lang_work_alias);
        $this->twig_env->addFilter($filter_to_absolute);

        $this->twig_env->addGlobal('host', $_SERVER['HTTP_HOST']);
        $this->twig_env->addGlobal('current_url', $this->current_url());
        $this->twig_env->addGlobal('secure', $_SERVER['HTTPS']);
        $this->twig_env->addGlobal('lang', $this->lang);
        $this->twig_env->addGlobal('explicit_lang_url', $this->explicit_lang_url);
        $this->twig_env->addGlobal('requested_work', $this->requested_work);
        $this->twig_env->addGlobal('available_langs', $this->available_langs);
    }

    public function run() {
        if ($this->requested_sitemap)
            header('Content-type: application/xml');

        ob_clean();
        ob_start('ob_gzhandler');

        if (!in_array($this->lang, $this->available_langs)) {
            // unsupported language
            http_response_code(303);
            header('Location: /' . $this->config['default_lang']);
        } else if ($this->requested_work) {
            // work detail page
            if (!$this->work_loader->exists($this->requested_work)) {
                $this->render_404();
            } else {
                $work = $this->work_loader->load($this->requested_work);
                $this->render_detail($this->requested_work, $work);
            }
        } else {
            // work listing
            foreach ($this->finder as $file) {
                $handle = null;
                $name = $file->getBasename('.work');
                $this->work_info_list[$name] = $this->work_loader->load_info($name, $handle);
                if ($this->requested_sitemap) {
                    $this->work_info_list[$name]['last_mod'] = gmdate("c", $file->getMTime());
                }
                fclose($handle);
            }

            // Sort items by their priority
            // @performance Probabbly a good adept for cacheing, as this
            //              will increse load time with each new work file.

            uasort($this->work_info_list, function ($aArray, $bArray) {
                $a = intval($aArray['sort']);
                $b = intval($bArray['sort']);

                if ($a == $b) return 0;
                return ($a > $b) ? -1 : 1;
            });

            // Group works by 'group' and 'year' for easier rendering.

            $grouped = array();
            foreach ($this->work_info_list as $filename => $element)
                $grouped[$element['group']][$element['year']][$filename] = $element;

            $this->work_info_list = $grouped;

            // Sort by year
            // @note This probably is not a bottleneck for faster
            //       listing times because it only sorts small amount
            //       of entries/years!

            krsort($this->work_info_list['personal'], SORT_NUMERIC);
            krsort($this->work_info_list['contract'], SORT_NUMERIC);

            if ($this->requested_sitemap) {
                $this->render_sitemap($this->work_info_list);
            } else {
                $this->render_list($this->work_info_list);
            }
        }

        ob_end_flush();
    }

    function render_detail($filename, $work) {
        echo $this->twig_env->render('detail.twig', [
            'filename' => $filename,
            'work' => $work,
            'title' => $work['info']['title']
        ]);
    }

    function render_list(array $work_info_list) {
        echo $this->twig_env->render('list.twig', [
            'title' => $this->translate('Portfolio'),
            'works' => $work_info_list
        ]);
    }

    function render_404() {
        http_response_code(404);
        echo $this->twig_env->render('error.twig', [
            'title' => $this->translate('error!'),
            'message' => $this->requested_work,
            'error' => 404
        ]);
    }

    function render_sitemap(array $work_info_list) {
        echo $this->twig_env->render('sitemap.twig', [
            'works' => $work_info_list
        ]);
    }

    function get_work_url($filename, $lang = null) {
        $lang = $lang ? $lang : $this->lang;
        $url_name = $this->get_lang_work_alias($filename, $lang);

        if ($this->rewrite)
            return $this->explicit_lang_url ? "/$this->lang/$url_name" : "/$url_name";
        else
            return $this->explicit_lang_url ? "/?lang=$this->lang&work=$this->work" : "/?work=$url_name";
    }

    function translate($string) {
        if (array_key_exists($string, $this->lang_map))
            return $this->lang_map[$string];

        return $string;
    }

    function current_url() {
        $work_name = $this->get_lang_work_alias($this->requested_work, $this->lang);

        return ($_SERVER['HTTPS'] ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . ($this->requested_work ? "/$work_name" : "/");
    }

    function to_absolute($url) {
        return ($_SERVER['HTTPS'] ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $url;
    }

    // @note probbably can be replaced with chained calls to to_absolute()
    function alternate_url($lang) {
        $work_name = $this->get_lang_work_alias($this->requested_work, $lang);

        return ($_SERVER['HTTPS'] ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . ($this->requested_work ? "/$lang/$work_name" : "/$lang") ;
    }

    function get_lang_work_alias($work, $lang) {
        if (!$work)
            return $work;

        if (!array_key_exists($lang, $this->aliases))
            return $work;

        if (!in_array($work, $this->aliases[$lang]))
            return $work;

        return array_search($work, $this->aliases[$lang]);
    }

}
