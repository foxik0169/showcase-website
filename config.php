<?php

return [
    'debug' => false,

    'root_dir' => __DIR__,
    'view_dir' => __DIR__ . '/view',
    'content_dir' => __DIR__ . '/content',
    'lang_dir' => __DIR__ . '/lang',

    'rewrite' => true,
    'explicit_lang_url' => array_key_exists('lang', $_GET),
    'lang' => array_key_exists('lang', $_GET) ? htmlspecialchars($_GET['lang']) : substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2),
    'default_lang' => 'sk'
];
