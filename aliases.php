<?php

/**
 * This file contains per-language aliases for each work file. Maps
 * only one, per-language alias to original .work file name.
 */

return [
    'sk' => [
        'ctvrta-vez' => '4th-tower',
        'klietka' => 'cage'
    ]
];
