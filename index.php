<?php

$loader = require_once __DIR__ . '/vendor/autoload.php';
$config = include_once __DIR__ . '/config.php';
$aliases = include_once __DIR__ . '/aliases.php';

use Showcase\App;

$app = new App($config, $aliases);
$app->run();
